package com.example.user.navdrawer;

import android.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import static com.example.user.navdrawer.R.*;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private String[] mPlanetTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mListView;
    private ActionBarDrawerToggle drawerListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);

        mPlanetTitles = getResources().getStringArray(array.abc);
        mDrawerLayout = (DrawerLayout) findViewById(id.drawer_layout);
        mListView = (ListView) findViewById(id.left_drawer);
        mListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,mPlanetTitles));

        mListView.setOnItemClickListener(this);


        drawerListener = new ActionBarDrawerToggle(this, mDrawerLayout,
                string.drawer_open, string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Toast.makeText(getApplicationContext(), " OPEN",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Toast.makeText(getApplicationContext(), "CLOSE",Toast.LENGTH_SHORT).show();
            }
        };
        mDrawerLayout.setDrawerListener(drawerListener);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, mPlanetTitles[position]+" selected.",Toast.LENGTH_SHORT).show();
        selectItem(position);
    }

    public void selectItem(int position){
        mListView.setItemChecked(position,true);
        setTitle(mPlanetTitles[position]);
    }
    public void setTitle(String title){
        getSupportActionBar().setTitle(title);
    }
}
